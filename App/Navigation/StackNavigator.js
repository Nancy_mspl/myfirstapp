import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../Screens/AuthScreens/Splash';
import Login from '../Screens/AuthScreens/Login'
const Stack = createStackNavigator();
const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default MainStackNavigator;
